# iOS Asset Generation

This is a modified version of the PSD provided by Michael Flarup [App Icon Template](http://appicontemplate.com/)

Changes:

* [Slicy](macrabbit.com/slicy/)-friendly layer names
* 6.1 and earlier App Icon (114x114, 57x57) for iPhone only
* 6.1 and earlier spotlight image (58x58, 29x29) for iPhone only

Planned Additions:
* default image template
* Review of iPad icon assets